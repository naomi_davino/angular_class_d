import { BrowserModule } from '@angular/platform-browser';
 import { NgModule } from '@angular/core';
 import { FormsModule }   from '@angular/forms';
 
 import { AppComponent } from './app.component';
 import { WelcomeComponent } from './welcome/welcome.component';
 import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import { NavComponent } from './nav/nav.component';
 import { LayoutModule } from '@angular/cdk/layout';
 import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
 import { MatToolbarModule } from '@angular/material/toolbar';
 import { MatButtonModule } from '@angular/material/button';
 import { MatSidenavModule } from '@angular/material/sidenav';
 import { MatIconModule } from '@angular/material/icon';
 import { MatListModule } from '@angular/material/list';
 import {MatExpansionModule} from '@angular/material/expansion';
 import {MatCardModule} from '@angular/material/card';
 import { BooksComponent } from './books/books.component';
 import { TemperaturesComponent } from './temperatures/temperatures.component';
 import { RouterModule, Routes } from '@angular/router';
import { TempformComponent } from './tempform/tempform.component';


 const appRoutes: Routes = [
   { path: 'books', component: WelcomeComponent },
   { path: 'temperatures/:temp/:city', component: TemperaturesComponent},
   { path: 'tempform', component: TempformComponent},
   { path: "",
     redirectTo: '/books',
     pathMatch: 'full'
   },
 ];
 
 @NgModule({
   declarations: [
     AppComponent,
     WelcomeComponent,
      NavComponent,
     BooksComponent,
     TemperaturesComponent,
     TempformComponent
   ],
   imports: [
     BrowserModule,
     BrowserAnimationsModule,
     LayoutModule,
     MatToolbarModule,
     MatButtonModule,
     MatSidenavModule,
     MatIconModule,
     MatListModule,
     MatExpansionModule,
     MatCardModule, 
     MatFormFieldModule, 
    MatSelectModule,
    MatInputModule,
    FormsModule,
     RouterModule.forRoot(
       appRoutes,
      { enableTracing: true } // <-- debugging purposes only
     )    
   ],
   providers: [],
   bootstrap: [AppComponent]
 })
 export class AppModule { }