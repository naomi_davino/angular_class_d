import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  likes:number = 0;
   
     addLikes(){
       this.likes++
     }
  
    constructor(private route: ActivatedRoute) { }
    temperature; 
    city;


  ngOnInit() {
    this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
  }

}
